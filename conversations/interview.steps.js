const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
module.exports = {
    flow: [showSteps]
}

const messages = {
    allStepsDescribed: "The interview process with Infusion is usually very consistent, however, for some roles it does change. Steps: \n\n" +
                        "1. Phone screen with a technical recruiter \n\n" +
                        "2. Tech1 - OOP interview over the phone \n\n" +
                        "3. Tech2  - language specific in-person interview (live coding) \n\n" +
                        "4. Final – Usually back-to-back with the tech2 – a mix of technical and behavioral questions \n\n" +
                        "5. Client Interview – We do not always have client interview… It depends on the role \n\n"
}

function showSteps(session, args, next) {
    session.send(messages.allStepsDescribed);
    session.endDialog();
}