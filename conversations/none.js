const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
module.exports = {
    flow: [sendInstructions, redirectConversation]
}

const messages = {
    failedToUnderstand: "Pardon me, I am unable to decipher your message. Here's what I can do instead:",
    botCapabilities: {
        "Directions": luisSettings.intents.directions,
        "Interview Preparation": "How do I prepare for the interview?",
        "Interview Steps": "How many steps are there?",
        "Job Descriptions": "Can I see my job description?"
    }
}

function sendInstructions(session, args, next) {
    session.sendTyping();
    builder.Prompts.choice(session, messages.failedToUnderstand, messages.botCapabilities, {
        listStyle: builder.ListStyle.button
    });
}

function redirectConversation(session, results, next) {
    if (results.response && results.response.entity) {
        // Manually assign a message and cancel dialog. This will trigger the luis matching again with this message.
        session.message.text = messages.botCapabilities[results.response.entity];
        // Canceldialog - 0 means cancel the first dialog on the stack and replaceItWith - '/'/
        session.cancelDialog(0, '/');
    } else {
        session.endDialog(); // End this dialog.
    }
}