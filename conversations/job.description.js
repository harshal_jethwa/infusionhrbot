const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
module.exports = {
    flow: [showDescription]
}

const messages = {
    descriptionsPosted: "All job descriptions are posted on our careers page: https://infusion.com/careers.html"
}

function showDescription(session, args, next) {
    session.send(messages.descriptionsPosted);
    session.endDialog();
}