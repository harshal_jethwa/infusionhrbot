const builder = require("botbuilder"); // By microsoft.
module.exports = {
    flow: [sayHello]
}

const messages = {
    greetings: "Howdy! Great weather outside eh? How can I assist you with the your Infusion Interview process?"
}

function sayHello(session, args, next) {
    session.sendTyping();
    session.endDialog(messages.greetings);
}