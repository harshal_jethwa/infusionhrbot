const builder = require("botbuilder"); // By microsoft.
const luisSettings = require("../luis.settings.js");
module.exports = {
    flow: [getInterviewStage, firstResponse]
}
var interviewStage = ""; // This is to avoid ReferenceError to interviewStage followup.message.
const messages = {
    whichStage: "Interview Prep is important! Do you know which stage you are going to be appearing for?",
    stages: {
        "phone": "Learn about infusion and revise up on general computer science techniques.",
        "tech1": "Look up some OOP concepts and practice small but challenging coding questions.",
        "tech2": "Practice white boarding and brush up on your main technology.",
        "final": "Practice behavioral questions along with computer science fundamentals.",
        "client": "Make sure you have a deep understanding of your primary technology."
    },
    followup: {
        message: `Furthermore, I can help you with the following things for your interview:`,
        options: ["More details", "Tips and Tricks", "Previously asked questions", "Mock interview", "Cancel"]
    }
}

function getInterviewStage(session, args, next) {
    session.sendTyping();
    session.dialogData.entities = args.entities;
    var interviewStage = builder.EntityRecognizer.findEntity(args.entities, luisSettings.entities.interviewStage.entityName);
    if (interviewStage) {
        next({
            response: {
                entity: interviewStage.entity
            }
        });
    } else {
        builder.Prompts.choice(session, messages.whichStage, messages.stages, {
            listStyle: builder.ListStyle.button
        });
    }
}

function firstResponse(session, results, next) {
    if (results.response.entity) {
        var interviewStage = results.response.entity; // For string interpolation.
        session.send(messages.stages[results.response.entity]);
        builder.Prompts.choice(session, messages.followup.message, messages.followup.options, {
            listStyle: builder.ListStyle.button
        });
    } else {
        session.next();
    }
}