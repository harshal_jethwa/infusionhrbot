var builder = require("botbuilder"); // By microsoft.
var restify = require("restify"); // To create a Rest service.

// Creating a connector. Used to connect channels. This is a simple console connector. Just so that we test is using the command window (console).
// For channels like slack/skype use chat connector.
// var connector = new builder.ConsoleConnector().listen();
var connector = new builder.ChatConnector();
// Create the bot.
var bot = new builder.UniversalBot(connector); // Universal - works with any and every connector.

// Setting up resitfy
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log(`${server.name} listening to ${server.url}`);
});
server.post('/api/messages', connector.listen()); // api/messages is just a standard. could be anything.

/**
 * Prompts
 * Free form - Text
 * Data-type detection - number, time
 * List of option - confirm, choice (diaply max 10 options. more than that messes up th UI/UX)
 * Media - attachment
 * 
 * Theres cards for Interaction.
 * 
 * Best Practices:
 * Minimize typing
 * Direct the user
 * Provide a list of commands
 */
bot.dialog('/', [
    function (session) {
        //session.send("Hello world");
        var thumbnail = new builder.ThumbnailCard(session);
        thumbnail.title("Harshal");
        thumbnail.images([builder.CardImage.create(session, "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50")]);
        thumbnail.subtitle("hjethwa");
        thumbnail.text("Consultant \nInfusion");
        thumbnail.tap(new builder.CardAction.openUrl(session, "http://www.google.ca"));

        var message = new builder.Message(session).attachments([thumbnail, thumbnail, thumbnail, thumbnail, thumbnail]).attachmentLayout('carousel');
        //session.send(message); //to send the cards.
        builder.Prompts.choice(session, "Which color?", ["red","green","blue"]); // To ask for clickable choices. 
    }
]);
