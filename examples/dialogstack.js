var builder = require("botbuilder"); // By microsoft.
var restify = require("restify"); // To create a Rest service.

// Creating a connector. Used to connect channels. This is a simple console connector. Just so that we test is using the command window (console).
// For channels like slack/skype use chat connector.
// var connector = new builder.ConsoleConnector().listen();
var connector = new builder.ChatConnector();
// Create the bot.
var bot = new builder.UniversalBot(connector); // Universal - works with any and every connector.

// Setting up resitfy
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log(`${server.name} listening to ${server.url}`);
});
server.post('/api/messages', connector.listen()); // api/messages is just a standard. could be anything.

/**
 * Understand dialog stack.
 * Session has a stack of dialogs.
 * session.beginDialog() - To add a dialog to the stack.
 * session.enddislalog() - to remove it from the stack.
 * 
 * Maintaining state:
 * This is done by the connector behind the scenes.
 * Saving data:
 * session.privateConversationData - only visible to current user. clear data if needed by calling endConversation().
 * session.conversationData - visible to all members of conversation
 * session.dialogData - data visible to current dialog
 * session.userData - data about user. persisted across the bot.
 * 
 * Therfore heirarchy (lifetime of a bot) - User data (top level) -> ConversationData (Under user data, multiple conversation per user) -> DialogData (under conversation, multiple dialog per conversation)
 */

// Add in the dialog
// bot.dialog('/', function(session) {
//     var userMesasge = session.message.text; // Typically this will be sent to luis.
//     session.send('Hello ' + userMesasge); // Session is communicate back to the user.
// });

// Waterfall - flow (1-way) of conversation.
// bot.dialog('/', [
//     function(session) {
//         builder.Prompts.text(session, 'Please enter your name: ');
//     },
//     function(session, result) {
//         session.send('Hello, ' + result.response);
//     } 
// ]);

// Dialog stack example.
// The stack is maintained in sessionState object in session. take a look at it (debug/watch it) to get an idea.
// remember this is a stack.
bot.dialog('/', [
    function (session) {
        session.beginDialog('/ensureProfile', session.userData.profile); // Second argument retrived as args.
        // Will add the ensureProfile waterfall on top the stack. So now the stack will have 2 items.
    },
    function (session, results) {
        session.userData.profile = results.response;
        session.send(`Hello ${session.userData.profile.name}! I love ${session.userData.profile.company}`);
    }
]);
bot.dialog('/ensureProfile', [
    function (session, args, next) { // Args sent by parent beginDialog.
        session.dialogData.profile = args || {};
        if (!session.dialogData.profile.name) {
            builder.Prompts.text(session, "What's your name?");
        } else {
            next(); // Can send data with next if needed. {reponse: "data"}. Will be retrived by result.data
        }
    },
    function (session, results, next) {
        if (results.response) {
            session.dialogData.profile.name = results.response;
        }
        if (!session.dialogData.profile.company) {
            builder.Prompts.text(session, "What company do you work for?");
        } else {
            next();
        }
    },
    function (session, results) {
        if (results.response) {
            session.dialogData.profile.company = results.response;
        }
        session.endDialogWithResult({response: session.dialogData.profile}); // removes it from the stack.
    }
]);